package model;

import java.util.Vector;

/**
 * Computer Player, an auto player with simple logic
 * 
 * @author dominic
 *
 */

public class ComputerPlayer extends Player 
{
	Vector<Integer> myDiscard = new Vector<Integer>(5);

	public ComputerPlayer(String name) 
	{
		super(name);
		myAmAI = true;
	}

	public Vector<Integer> selectCardsToDiscard() 
	{
		if (myHand.isRoyalFlush() == true || 
				myHand.isStraightFlush() == true || 
				myHand.isFourOfKind() == true || 
				myHand.isFullHouse() == true ||
				myHand.isFlush() == true || 
				myHand.isStraight() == true) 
		{
			myHand.discard(myDiscard);
		} 
		else if (myHand.isThreeOfKind() == true) 
		{
			myHand.orderCards();

			if (myHand.getCards().get(0).getType() == myHand.getCards().get(1).getType()
					&& myHand.getCards().get(1).getType() == myHand.getCards().get(2).getType()
					&& myHand.getCards().get(2).getType() != myHand.getCards().get(3).getType()
					&& myHand.getCards().get(3).getType() != myHand.getCards().get(4).getType()) 
			{
				if (myHand.getCards().get(0).getSuit() != myHand.getCards().get(1).getSuit()
						&& myHand.getCards().get(1).getSuit() != myHand.getCards().get(2).getSuit()) 
				{
					myDiscard.add(3);
					myDiscard.add(4);
					return myDiscard;
				}
			} 
			else if (myHand.getCards().get(1).getType() == myHand.getCards().get(2).getType()
					&& myHand.getCards().get(2).getType() == myHand.getCards().get(3).getType()
					&& myHand.getCards().get(0).getType() != myHand.getCards().get(1).getType()
					&& myHand.getCards().get(3).getType() != myHand.getCards().get(4).getType()) 
			{
				if (myHand.getCards().get(1).getSuit() != myHand.getCards().get(2).getSuit()
						&& myHand.getCards().get(2).getSuit() != myHand.getCards().get(3).getSuit()) 
				{
					myDiscard.add(0);
					myDiscard.add(4);
					return myDiscard;
				}
			} 
			else if (myHand.getCards().get(2).getType() == myHand.getCards().get(3).getType()
					&& myHand.getCards().get(3).getType() == myHand.getCards().get(4).getType()
					&& myHand.getCards().get(0).getType() != myHand.getCards().get(1).getType()
					&& myHand.getCards().get(1).getType() != myHand.getCards().get(2).getType()) 
			{
				if (myHand.getCards().get(2).getSuit() != myHand.getCards().get(3).getSuit()
						&& myHand.getCards().get(3).getSuit() != myHand.getCards().get(4).getSuit()) 
				{
					myDiscard.add(0);
					myDiscard.add(1);
					return myDiscard;
				}
			}
			myHand.discard(myDiscard);

		} 
		else if (myHand.isTwoPair() == true) 
		{
			if (myHand.getCards().get(0).getType() == myHand.getCards().get(1).getType()
					&& myHand.getCards().get(2).getType() == myHand.getCards().get(3).getType()
					&& myHand.getCards().get(3).getType() != myHand.getCards().get(4).getType()
					&& myHand.getCards().get(1).getType() != myHand.getCards().get(4).getType()) 
			{
				if (myHand.getCards().get(0).getSuit() != myHand.getCards().get(1).getSuit()
						&& myHand.getCards().get(2).getSuit() != myHand.getCards().get(3).getSuit()) 
				{
					myDiscard.removeAllElements();
					myDiscard.add(4);
					return myDiscard;
				}
			} 
			else if (myHand.getCards().get(0).getType() == myHand.getCards().get(1).getType()
					&& myHand.getCards().get(3).getType() == myHand.getCards().get(4).getType()
					&& myHand.getCards().get(1).getType() != myHand.getCards().get(2).getType()
					&& myHand.getCards().get(2).getType() != myHand.getCards().get(3).getType()) 
			{
				if (myHand.getCards().get(1).getSuit() != myHand.getCards().get(2).getSuit()
						&& myHand.getCards().get(2).getSuit() != myHand.getCards().get(3).getSuit()) 
				{
					myDiscard.removeAllElements();
					myDiscard.add(2);
					return myDiscard;
				}
			} 
			else if (myHand.getCards().get(1).getType() == myHand.getCards().get(2).getType()
					&& myHand.getCards().get(3).getType() == myHand.getCards().get(4).getType()
					&& myHand.getCards().get(0).getType() != myHand.getCards().get(1).getType()
					&& myHand.getCards().get(0).getType() != myHand.getCards().get(3).getType()) 
			{
				if (myHand.getCards().get(1).getSuit() != myHand.getCards().get(2).getSuit()
						&& myHand.getCards().get(3).getSuit() != myHand.getCards().get(4).getSuit()) 
				{
					myDiscard.removeAllElements();
					myDiscard.add(0);
					return myDiscard;
				}
			}

			myHand.discard(myDiscard);
		} 
		else if (myHand.isPair() == true) 
		{
			if (myHand.getCards().get(0).getType() == myHand.getCards().get(1).getType()
					&& myHand.getCards().get(0).getSuit() != myHand.getCards().get(1).getSuit()) 
			{
				myDiscard.add(2);
				myDiscard.add(3);
				myDiscard.add(4);
				return myDiscard;
			} 
			else if (myHand.getCards().get(1).getType() == myHand.getCards().get(2).getType()
					&& myHand.getCards().get(1).getSuit() != myHand.getCards().get(2).getSuit()) 
			{
				myDiscard.add(0);
				myDiscard.add(3);
				myDiscard.add(4);
				return myDiscard;
			} 
			else if (myHand.getCards().get(2).getType() == myHand.getCards().get(3).getType()
					&& myHand.getCards().get(2).getSuit() != myHand.getCards().get(3).getSuit()) 
			{
				myDiscard.add(0);
				myDiscard.add(1);
				myDiscard.add(4);
				return myDiscard;
			} 
			else if (myHand.getCards().get(3).getType() == myHand.getCards().get(4).getType()
					&& myHand.getCards().get(3).getSuit() != myHand.getCards().get(4).getSuit()) 
			{
				myDiscard.add(0);
				myDiscard.add(1);
				myDiscard.add(2);
				return myDiscard;
			}
			myHand.discard(myDiscard);
		}
		else if (myHand.isHighCard() == true) 
		{

			myDiscard.add(0);
			myDiscard.add(1);
			myDiscard.add(2);

			myHand.discard(myDiscard);
		}
		return myDiscard;
	}

}
