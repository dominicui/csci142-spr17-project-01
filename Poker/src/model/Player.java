package model;


/**
 * Player class
 * set name, make functional 
 * 
 * @author dominic
 *
 */

public class Player
{
	private static final String DEFAULT_NAME = "John Cena";
	private String myName;
	private int myNumberWins = 0;
	protected boolean myAmAI;
	protected PokerHand myHand;
   
	public Player(String name)
	{
		myName = name;
		myHand = new PokerHand(5);
		myAmAI = false;
	}

	public boolean validateName(String name)
	{
		char[] check = myName.toCharArray();
		for(int i = 0; i < check.length; i++)
		{
			if(Character.isLowerCase(check[i]) || Character.isUpperCase(check[i]) || Character.isDigit(check[i]))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		return false;
	}

	public int incrementNumberWins()
	{
		myNumberWins++;
		return myNumberWins;
	}

	public String toString()
	{
		return null;
	}

	public Object clone()
	{
		return null;
	}

	public Hand getHand()
	{
		return myHand;
	}

	public String getName()
	{
		return myName;
	}

	public int getNumberWins()
	{
		return myNumberWins;
	}

	public boolean getAmAI()
	{	
		return myAmAI;
	}

}
