package model;

/**
 * PokerNodel class
 * 
 * project developed by dominic self.
 * 
 * @author dominic
 *
 */
		
public class PokerModel
{
	private Player[] myPlayer = new Player[2];
	private int myIndexPlayerup;
	private int myMaxRounds;
	private int myRound;
	private ComputerPlayer myCP;
	private Hand myHand;
	private Deck myDeck;
	
	public PokerModel(Player player)
	{
		myDeck = new Deck();
		myHand = new Hand(5);
		myCP = new ComputerPlayer("AI");
		
		myPlayer[0] = player;
		myPlayer[1] = myCP;
		
		myIndexPlayerup = 0;
		myMaxRounds = 0;
		myRound = 0;
	}

	public int switchTurns()
	{
		if(myIndexPlayerup == 0)
		{
			return myIndexPlayerup = 1;
		}
		else
		{
			return myIndexPlayerup = 0;
		}
	}

	public void dealCards()
	{
		for(int i = 0; i < 2; i++)
		{
			int size = myPlayer[i].getHand().getCards().size();
			int capacity = myPlayer[i].getHand().getCards().capacity();
			
			for(int j = size; j < capacity; j++)
			{
				Card card = myDeck.draw();
				myPlayer[i].getHand().getCards().add(card);
			}
		}
	}

	public Player determineWinner()
	{
		for(int i = 0; i < 2; i++)
		{
			int size = myPlayer[i].getHand().getCards().size();
			int capacity = myPlayer[i].getHand().getCards().capacity();
			
			for(int j = size; j < capacity; j++)
			{
				Card card = myDeck.draw();
				myPlayer[i].getHand().getCards().add(card);
			}
		}
		return null;
	}

	/**
	 * note: do not make hand = null;
	 * @return
	 */
	
	public boolean resetGame()
	{
		myDeck = new Deck();
		myPlayer[0].getHand().getCards().clear();
		myPlayer[1].getHand().getCards().clear();
		
		myIndexPlayerup = 0;
		myMaxRounds = 0;
		myRound = 0;
		return true;
	}

	public Player getPlayerUp()
	{
		return myPlayer[myIndexPlayerup];
	}

	public Player getPlayer(int index)
	{
		return myPlayer[index];
	}

	public int getIndexPlayerUp()
	{
		return myIndexPlayerup;
	}

}
