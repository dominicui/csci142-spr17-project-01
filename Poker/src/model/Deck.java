package model;

import java.awt.Image;
import java.util.Random;
import java.util.Vector;

/**
 * Deck class
 * new deck, shuffle deck, draw card
 * 
 * @author dominic
 *
 */

public class Deck
{
	private Vector<Card> myCards;
	private Vector<CardType> myCardType;
	private Vector<CardSuit> myCardSuit;
	private CardSuit mySuit;
	private CardType myType;
	private Card myCard;
	private Image myImage;
	private int myCardsinDeck = 52;
	private int myDrawNum;
	private boolean myConstruct;
	private Card myDrawCard;

	public Deck()
	{
	    orderingCard();
		
		myCards = new Vector<Card>(52);
		
		for(int j = 0; j < myCardSuit.capacity(); j++)
		{
			for(int x = 0; x < myCardType.capacity(); x++)
			{
				myCard = new Card(myCardSuit.elementAt(j), myCardType.elementAt(x), myImage);
				myCards.add(myCard);
			}
		}
		
		myConstruct = true;
	}

	public boolean constructDeck()
	{
		return myConstruct;
	}

	public Card draw()
	{
		Random draw = new Random(myCardsinDeck);
		myDrawNum = Math.abs(draw.nextInt(myCardsinDeck));
		myDrawCard = myCards.elementAt(myDrawNum);
		myCards.remove(myDrawNum);
		return myDrawCard;
	}

	public boolean shuffle()
	{
		Vector<Card> shuffle = new Vector<Card>(52);

		for (int i = myCards.size(); i > 0; i--) 
		{
			Random rand = new Random();
			int r = Math.abs(rand.nextInt(i));

			shuffle.add(myCards.get(r));	
			myCards.remove(r);
			
		}
		return myCards.equals(shuffle);
	}

	public String toString()
	{
		return null;
	}

	public Object clone()
	{
		return myCards;
	}
	
	/**
	 * helper method to build a new deck
	 */
	
	public void orderingCard()
	{
		myCardSuit = new Vector<CardSuit>(4);
		myCardType = new Vector<CardType>(13);
		
		myCardSuit.add(mySuit.CLUBS);
		myCardSuit.add(mySuit.DIAMONDS);
		myCardSuit.add(mySuit.HEARTS);
		myCardSuit.add(mySuit.SPADES);
		
		myCardType.add(myType.ACE);
		myCardType.add(myType.TWO);
		myCardType.add(myType.THREE);
		myCardType.add(myType.FOUR);
		myCardType.add(myType.FIVE);
		myCardType.add(myType.SIX);
		myCardType.add(myType.SEVEN);
		myCardType.add(myType.EIGHT);
		myCardType.add(myType.NINE);
		myCardType.add(myType.TEN);
		myCardType.add(myType.JACK);
		myCardType.add(myType.QUEEN);
		myCardType.add(myType.KING);
	}
}
