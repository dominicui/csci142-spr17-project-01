package model;

import java.awt.Image;

/**
 * Card class;
 * 
 * @author dominic
 *
 */

public class Card
{
    private CardSuit mySuit;
    private CardType myType;
	private Image myImage;
	private boolean myIsFaceUp = false;
	private boolean myIsSelected = false;
	
	public Card(CardSuit suit, CardType type, Image image)
	{
		mySuit = suit;
		myType = type;
		myImage = image;
	}

	public boolean isFaceUp()
	{
		return myIsFaceUp;	
	}

	public boolean isSelected()
	{
		return myIsSelected;
	}
	public void flip()
	{
		myIsFaceUp = true;
	}

	public void toggleSelected()
	{
		if(this.isSelected() == false)
		{
			myIsSelected = true;
			return;
		}
		else if(this.isSelected() == true)
		{
			myIsSelected = false;
			return;
		}
		
	}
	
	public int compareTo(Card card)
	{
		int num = 0;
		
		if(this.getSuit() == card.getSuit() && this.getType() != card.getType())
		{
			num = -1;
		}
		else if(this.getSuit() != card.getSuit() && this.getType() != card.getType())
		{
			num = 1;
		}
		return num;
	}
	
	public int getType()
	{
		return myType.getType();
	}

	public String getSuit()
	{
		return mySuit.getSuit();
	}

	public Image getImage()
	{
		return null;
	}

	public String toString()
	{
		return null;
	}

	public Object clone()
	{
		return null;
	}

}
