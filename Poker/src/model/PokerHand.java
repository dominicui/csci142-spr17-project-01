package model;

/**
 * PokerHand class
 * determining PokerHand and get ranking
 * 
 * @author dominic
 *
 */

public class PokerHand extends Hand
{
	private int myNumberCards;
	private int myMaxNumberCards;
	private PokerHandRanking myRanking;
	private int myGetRank;
    private boolean myCheck = false;
	
	public PokerHand(int maxNum)
	{
		super(maxNum);
		myMaxNumberCards = maxNum;
	}

	public PokerHandRanking determineRanking()
	{
		return myRanking;
	}

	public int compareTo(PokerHand pokerHand)
	{
		return 0;
	}

	public String toString()
	{
		return null;
	}

	public int getRanking()
	{
		if(determineRanking() == PokerHandRanking.ROYAL_FLUSH)
		{
			myGetRank = PokerHandRanking.ROYAL_FLUSH.getRank();
		}
		else if(determineRanking() == PokerHandRanking.STRAIGHT_FLUSH)
		{
			myGetRank = PokerHandRanking.STRAIGHT_FLUSH.getRank();
		}
		else if(determineRanking() == PokerHandRanking.FOUR_OF_KIND)
		{
			myGetRank = PokerHandRanking.FOUR_OF_KIND.getRank();
		}
		else if(determineRanking() == PokerHandRanking.FULL_HOUSE)
		{
			myGetRank = PokerHandRanking.FULL_HOUSE.getRank();
		}
		else if(determineRanking() == PokerHandRanking.FLUSH)
		{
			myGetRank = PokerHandRanking.FLUSH.getRank();
		}
		else if(determineRanking() == PokerHandRanking.STRAIGHT)
		{
			myGetRank = PokerHandRanking.STRAIGHT.getRank();
		}
		else if(determineRanking() == PokerHandRanking.THREE_OF_KIND)
		{
			myGetRank = PokerHandRanking.THREE_OF_KIND.getRank();
		}
		else if(determineRanking() == PokerHandRanking.TWO_PAIR)
		{
			myGetRank = PokerHandRanking.TWO_PAIR.getRank();
		}
		else if(determineRanking() == PokerHandRanking.PAIR)
		{
	    	myGetRank = PokerHandRanking.PAIR.getRank();
		}
		else if(determineRanking() == PokerHandRanking.HIGH_CARD)
		{
			myGetRank = PokerHandRanking.HIGH_CARD.getRank();
		}

		return myGetRank;
	}

	public int getNumberCards()
	{
		return myNumberCards = myCards.size();
	}

	public int getMaxNumberCards()
	{
		return myMaxNumberCards = myCards.capacity();
	}

	/**
	 * note: make myCards in Hand visible, and directly access it
	 * Some method below I use loop to do it, because it is messy using loop
	 * 
	 * @return
	 */
	
	public boolean isHighCard()
	{
		myRanking = PokerHandRanking.HIGH_CARD;
		return true;
	}
	
	public boolean isPair()
	{		
        if(myCards.get(0).getType() == myCards.get(1).getType()
				|| myCards.get(1).getType() == myCards.get(2).getType()
				|| myCards.get(2).getType() == myCards.get(3).getType()
				|| myCards.get(3).getType() == myCards.get(4).getType())
		{
			if(myCards.get(0).getSuit() != myCards.get(1).getSuit()
					|| myCards.get(1).getSuit() != myCards.get(2).getSuit()
					|| myCards.get(2).getSuit() != myCards.get(3).getSuit()
					|| myCards.get(3).getSuit() != myCards.get(4).getSuit())
			{
				myRanking = PokerHandRanking.PAIR;
				myCheck = true;
			}
			else
			{
				myCheck = false;
			}
		}
        else
		{
			myCheck = false;
		}		
		return myCheck;
	}

	public boolean isTwoPair()
	{		
		if(myCards.get(0).getType() == myCards.get(1).getType()
			&& myCards.get(2).getType() == myCards.get(3).getType()
			&& myCards.get(3).getType() != myCards.get(4).getType()
			&& myCards.get(1).getType() != myCards.get(4).getType())
		{
			if(myCards.get(0).getSuit() != myCards.get(1).getSuit()
				&& myCards.get(2).getSuit() != myCards.get(3).getSuit())
			{
				myRanking = PokerHandRanking.TWO_PAIR;
				myCheck = true;
			}else
			{
				myCheck = false;
			}
		}
		else if(myCards.get(0).getType() == myCards.get(1).getType()
			&& myCards.get(3).getType() == myCards.get(4).getType()
			&& myCards.get(1).getType() != myCards.get(2).getType()
			&& myCards.get(2).getType() != myCards.get(3).getType())
		{
				if(myCards.get(1).getSuit() != myCards.get(2).getSuit()
					&& myCards.get(2).getSuit() != myCards.get(3).getSuit())
				{
					myRanking = PokerHandRanking.TWO_PAIR;
					myCheck = true;
				}
				else
				{
					myCheck = false;
				}
		}
		else if(myCards.get(1).getType() == myCards.get(2).getType()
				&& myCards.get(3).getType() == myCards.get(4).getType()
				&& myCards.get(0).getType() != myCards.get(1).getType()
				&& myCards.get(0).getType() != myCards.get(3).getType())
			{
					if(myCards.get(1).getSuit() != myCards.get(2).getSuit()
						&& myCards.get(3).getSuit() != myCards.get(4).getSuit())
					{
						myRanking = PokerHandRanking.TWO_PAIR;
						myCheck = true;
					}
					else
					{
						myCheck = false;
					}
			}	
		return myCheck;
	}

	public boolean isThreeOfKind()
	{		
		if(myCards.get(0).getType() == myCards.get(1).getType()
			&& myCards.get(1).getType() == myCards.get(2).getType()
			&& myCards.get(2).getType() != myCards.get(3).getType()
			&& myCards.get(3).getType() != myCards.get(4).getType())
		{
			if(myCards.get(0).getSuit() != myCards.get(1).getSuit()
				&& myCards.get(1).getSuit() != myCards.get(2).getSuit())
			{
				myRanking = PokerHandRanking.THREE_OF_KIND;
				myCheck = true;
			}
			else
			{
				myCheck = false;
			}
		}
		else if(myCards.get(1).getType() == myCards.get(2).getType()
			&& myCards.get(2).getType() == myCards.get(3).getType()
			&& myCards.get(0).getType() != myCards.get(1).getType()
			&& myCards.get(3).getType() != myCards.get(4).getType())
		{
				if(myCards.get(1).getSuit() != myCards.get(2).getSuit()
					&& myCards.get(2).getSuit() != myCards.get(3).getSuit())
				{
					myRanking = PokerHandRanking.THREE_OF_KIND;
					myCheck = true;
				}
				else
				{
					myCheck = false;
				}
		}
		else if(myCards.get(2).getType() == myCards.get(3).getType()
				&& myCards.get(3).getType() == myCards.get(4).getType()
				&& myCards.get(0).getType() != myCards.get(1).getType()
				&& myCards.get(1).getType() != myCards.get(2).getType())
			{
					if(myCards.get(2).getSuit() != myCards.get(3).getSuit()
						&& myCards.get(3).getSuit() != myCards.get(4).getSuit())
					{
						myRanking = PokerHandRanking.THREE_OF_KIND;
						myCheck = true;
					}
					else
					{
						myCheck = false;
					}
			}

		return myCheck;
	}

	public boolean isStraight()
	{		
		for(int j = 0; j < myCards.size()-1; j++)
		{
			if(myCards.elementAt(j).getType() == myCards.elementAt(j+1).getType()-1)
			{
				myRanking = PokerHandRanking.STRAIGHT;
				myCheck = true;
			}
			else
			{
				myCheck = false;
				break;
			}
		}
		return myCheck;
	}

	public boolean isFlush()
	{		
		for(int j = 0; j < myCards.size()-1; j++)
		{
			if(myCards.elementAt(j).getSuit() == myCards.elementAt(j+1).getSuit())
			{
				myRanking = PokerHandRanking.FLUSH;
				myCheck = true;
			}
			else
			{
				myCheck = false;
				break;
			}
		}
		return myCheck;
	}

	public boolean isFullHouse()
	{		
		if(myCards.get(0).getType() == myCards.get(1).getType()
			&& myCards.get(1).getType() == myCards.get(2).getType()
			&& myCards.get(3).getType() == myCards.get(4).getType()
			&& myCards.get(2).getType() != myCards.get(3).getType())
		{
			if(myCards.get(0).getSuit() != myCards.get(1).getSuit()
				&& myCards.get(1).getSuit() != myCards.get(2).getSuit()
			    && myCards.get(3).getSuit() != myCards.get(4).getSuit())
			{
				myRanking = PokerHandRanking.FULL_HOUSE;
				myCheck = true;
			}
			else
			{
				myCheck = false;
			}
		}
		else if(myCards.get(0).getType() == myCards.get(1).getType()
		    && myCards.get(2).getType() == myCards.get(3).getType()
			&& myCards.get(3).getType() == myCards.get(4).getType()
			&& myCards.get(1).getType() != myCards.get(2).getType())
		{
				if(myCards.get(0).getSuit() != myCards.get(1).getSuit()
					&& myCards.get(2).getSuit() != myCards.get(3).getSuit()
					&& myCards.get(3).getSuit() != myCards.get(4).getSuit())
				{
					myRanking = PokerHandRanking.FULL_HOUSE;
					myCheck = true;
				}
				else
				{
					myCheck = false;
				}
		}
		return myCheck;
	}

	public boolean isFourOfKind()
	{		
		if(myCards.get(0).getType() == myCards.get(1).getType()
			&& myCards.get(1).getType() == myCards.get(2).getType()
			&& myCards.get(2).getType() == myCards.get(3).getType()
			&& myCards.get(3).getType() != myCards.get(4).getType())
		{
			if(myCards.get(0).getSuit() != myCards.get(1).getSuit()
				&& myCards.get(1).getSuit() != myCards.get(2).getSuit()
				&& myCards.get(2).getSuit() != myCards.get(3).getSuit())
			{
				myRanking = PokerHandRanking.FOUR_OF_KIND;
				myCheck = true;
			}
			else
			{
				myCheck = false;
			}
		}
		else if(myCards.get(1).getType() == myCards.get(2).getType()
			&& myCards.get(2).getType() == myCards.get(3).getType()
			&& myCards.get(3).getType() == myCards.get(4).getType()
			&& myCards.get(0).getType() != myCards.get(1).getType())
		{
				if(myCards.get(1).getSuit() != myCards.get(2).getSuit()
					&& myCards.get(2).getSuit() != myCards.get(3).getSuit()
					&& myCards.get(3).getSuit() != myCards.get(4).getSuit())
				{
					myRanking = PokerHandRanking.FOUR_OF_KIND;
					myCheck = true;
				}
				else
				{
					myCheck = false;
				}
		}
		return myCheck;
	}

	public boolean isStraightFlush()
	{		
		for(int i = 1; i < myCards.size()-1; i++)
		{
			String suit = myCards.get(0).getSuit();
			
			if(myCards.get(i).getSuit() == suit)
			{
				for(int j = 0; j < myCards.size()-1; j++)
				{
					if(myCards.get(j).getType() == myCards.get(j+1).getType()-1)
					{
						myRanking = PokerHandRanking.STRAIGHT_FLUSH;
						myCheck = true;
					}
					else
					{
						myCheck = false;
						break;
					}
				}
			}
			else
			{
				myCheck = false;
				break;
			}
		}
		return myCheck;
	}

	public boolean isRoyalFlush()
	{		
		for(int i = 1; i < myCards.size()-1; i++)
		{
			String suit = myCards.get(0).getSuit();
			
			if(myCards.get(i).getSuit() == suit)
			{
				for(int j = 0; j < myCards.size()-1; j++)
				{
					if(myCards.get(0).getType() == 10 &&
							myCards.get(j).getType() == myCards.get(j+1).getType()-1)
					{
						myRanking = PokerHandRanking.ROYAL_FLUSH;
						myCheck = true;
					}
					else
					{
						myCheck = false;
						break;
					}
				}
			}
			else
			{
				myCheck = false;
				break;
			}
		}
		return myCheck;
	}

}
