package model;

import java.util.Vector;

/**
 * Hand class
 * add card to hand, discard, sort card
 * 
 * @author dominic
 *
 */

public class Hand
{
	protected Vector<Card> myCards;
	private Vector<Card> myDiscard;

	public Hand(int maxNum)
	{
		myCards = new Vector<Card>(maxNum);
	}

	public Vector<Card> getCards()
	{
		return myCards;
	}

	public int getNumberCardsInHand()
	{
		return myCards.size();
	}

	public boolean add(Card card)
	{
		boolean add = true;
		
		if(myCards.size() == 0)
		{
			myCards.add(card);
			return add = true;
		}
		else if(myCards.size() < 5)
		{
			for(int i = 0; i < myCards.size(); i++)
			{
				if(card != getCards().get(i))
				{
					myCards.add(card);
					return add = true;
				}
				else
				{
					return add = false;
				}
			}
		}
		else
		{
			return add = false;
		}
		return add;
	}

	/**
	 * note: discard from the last to first
	 * 
	 * @param indices
	 * @return
	 */
	
	public Vector<Card> discard(Vector<Integer> indices)
	{
		myDiscard = new Vector<Card>(5);
		for(int i = indices.size()-1; i >= 0; i--)
		{
			int ind = indices.elementAt(i);
		    
		    myDiscard.add(myCards.elementAt(ind));
		    myCards.remove(ind);
		}
		return myDiscard;
	}

	public String toString()
	{
		return null;
	}

	public void orderCards()
	{
		int index;
		int min;

		Vector<Card> sortedCards = new Vector<Card>(5);
		
		for(int unSorted = myCards.size(); unSorted > 0; unSorted--)
		{	
			min = 0;
			for(index = 1; index < unSorted; index++)
			{
				int type1 = myCards.elementAt(min).getType();
				int type2 = myCards.elementAt(index).getType();
				if(type1 > type2)
				{
					min = index;
				}
			}
			sortedCards.add(myCards.elementAt(min));
			myCards.removeElementAt(min);
		}
		myCards = sortedCards;
	}
	
}
