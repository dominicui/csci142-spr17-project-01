package modeltest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.Card;
import model.Deck;
/**
 * Deck Test cases to test the functionality of the Deck class
 * 
 * Date : 2/24/16
 * 
 * @author Spring 2016 class
 * 
 *
 */
public class DeckTests
{

	private Deck myDeck;

	@Before
	public void setup()
	{
		myDeck = new Deck();
	}

	/**
	 * Testing to see if the deck was shuffled by cloning the deck then
	 * comparing it to the shuffled deck
	 */
	
	@Test
	public void testIfShuffled()
	{
		Object myDeckCopy = myDeck.clone();
		myDeck.shuffle();
		boolean test = (myDeckCopy.equals(myDeck));
		assertTrue("The deck was shuffled", !test);
	}

	/**
	 * Test to see if a card was drawn by calling .draw()
	 */
	
	@Test
	public void testIfDrawn()
	{
		String card = myDeck.draw().getSuit();
		
		boolean test = false;
		if (card.equals("Spades") || card.equals("Hearts") || card.equals("Diamonds") || card.equals("Clubs")) 
		{
			test = true;
		}
		
		assertTrue("It is a card!", test);
	}

}
