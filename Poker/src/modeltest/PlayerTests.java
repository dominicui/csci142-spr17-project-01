package modeltest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.Player;
/**
 * Test the functionality of the Player class
 * 
 * Date : 2/24/16
 * 
 * @author Spring 2016 class
 *
 */
public class PlayerTests
{
	private Player myPlayer;

	@Before
	public void setup()
	{
		myPlayer = new Player("ChadErica");
	}

	/**
	 * Testing to see if the validateName() function works by calling the
	 * validate name with the name given by name()
	 */
	@Test
	public void testIfVaildName()
	{
		String name = myPlayer.getName();
		boolean test = myPlayer.validateName(name);
		//assertFalse("Name has a error in it", test);
		assertTrue("The Name works", test);
	}

	/**
	 * Testing to see if the Player is an AI or not by calling the .getAmAI()
	 * Function
	 */
	@Test
	public void testAmAI()
	{
		boolean test = myPlayer.getAmAI();
		assertFalse("The Player should not be an AI", test);
	}

	/**
	 * Test to see if there are more than 5 wins in one game by calling
	 * .getNumberWins The number should be less than 5
	 */
	@Test
	public void testNumberWins()
	{
		int wins = myPlayer.getNumberWins();
		assertFalse("More than 5 wins", wins > 5);
	}

}
